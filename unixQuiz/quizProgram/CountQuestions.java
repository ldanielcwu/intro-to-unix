import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CountQuestions {
    private static final String questionsFileName = "questions.txt";

    public static void main(String[] args) {
        File f = new File(questionsFileName);
        try {
            Scanner s = new Scanner(f);
            int lines = 0;
            while (s.hasNextLine()) {
                String nextLine = s.nextLine();
                lines += 1;
            }
            System.out.println(questionsFileName + " has " + lines + " lines.");

        } catch (FileNotFoundException e) {
            System.err.println("File not found: " + questionsFileName);
            System.exit(1);
        }
    }
}
