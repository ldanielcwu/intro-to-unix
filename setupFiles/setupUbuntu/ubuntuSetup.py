#!/usr/bin/python

import subprocess

###########################################################################
# Generic shell commands
###########################################################################
def killall(process):
    subprocess.call(["sudo", "killall", "-9", process])

def rm_rf(path):
    subprocess.call(["sudo", "rm", "-rf", path, "2>/dev/null"])

###########################################################################
# Generic apt commands
###########################################################################
def addRepository(repository):
    subprocess.call(["sudo", "add-apt-repository", "-y", repository])

###########################################################################
# Apt-Fast commands
###########################################################################
def autoclean():
    subprocess.call("sudo apt-fast autoclean".split())

def autoremove():
    subprocess.call("sudo apt-fast -y autoremove --purge".split())

def clean():
    subprocess.call("sudo apt-fast clean".split())

def install(package):
    subprocess.call(["sudo", "apt-fast", "-y", "install", package])

def purge(package_list):
    purge_command = ["sudo", "apt-fast", "-y", "purge"]
    for package in package_list:
        purge_command.append(package)
    subprocess.call(purge_command)

def update():
    subprocess.call("sudo apt-fast update".split())

###########################################################################
# Apt-Get commands
###########################################################################
def updateSlow():
    subprocess.call("sudo apt-get update".split())

def installSlow(package):
    subprocess.call(["sudo", "apt-get", "-y", "install", package])

###########################################################################
# Other install commands
###########################################################################
def gemInstall(gem):
    subprocess.call(["sudo", "gem", "install", gem])
    subprocess.call(["gem", "install", gem])

###########################################################################
# Setup-specific commands
###########################################################################
def addGoogleKey():
    _wget = subprocess.Popen(["wget", "-q", "-O", "-", "https://dl-ssl.google.com/linux/linux_signing_key.pub"],
                            stdout=subprocess.PIPE)
    _output = subprocess.check_output(["sudo", "apt-key", "add", "-"],
                                     stdin=_wget.stdout)
    _wget.wait()
    print _output

def addSpotifyKey():
    subprocess.call(["sudo", "apt-key", "adv", "--keyserver", "keyserver.ubuntu.com", "--recv-keys", "94558F59"])

def addLine(file_name, new_line_contents):
    _result = subprocess.Popen(["sudo", "cat", file_name],
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE)

    _out, _err = _result.communicate()
    _file_lines = _out.split("\n")

    _has_line = False
    for _line in _file_lines:
        if _line == new_line_contents:
            _has_line = True
            break

    if _has_line:
        print "Error: File contains input line - not appending line to file"
    else:
        _echo_command = "echo \"" + new_line_contents + "\" >> " + file_name
        subprocess.call(["sudo", "sh", "-c", _echo_command])
        print "Successfully appened line contents to file"

def enableAutoInstallJava1():
    _echo = subprocess.Popen(["echo", "debconf",
                              "shared/accepted-oracle-license-v1-1",
                              "select", "true"], stdout=subprocess.PIPE)
    _debconf = subprocess.check_output(["sudo", "debconf-set-selections"],
                                       stdin=_echo.stdout)
    _echo.wait()
    print _debconf

def enableAutoInstallJava2():
    _echo = subprocess.Popen(["echo", "debconf",
                              "shared/accepted-oracle-license-v1-1",
                              "seen", "true"], stdout=subprocess.PIPE)
    _debconf = subprocess.check_output(["sudo", "debconf-set-selections"],
                                       stdin=_echo.stdout)
    _echo.wait()
    print _debconf

def enableAutoInstallJava():
    enableAutoInstallJava1()
    enableAutoInstallJava2()

def installHeroku():
    _getHerokuScript = subprocess.Popen(["wget", "-qO-",
                              "https://toolbelt.heroku.com/install-ubuntu.sh"],
                             stdout=subprocess.PIPE)
    _install = subprocess.check_output(["sh"], stdin=_getHerokuScript.stdout)
    _getHerokuScript.wait()
    print _install

def installRvm():
    install("curl")
    _getRVM = subprocess.Popen(["curl", "-L", "https://get.rvm.io"],
                             stdout=subprocess.PIPE)
    _install = subprocess.check_output(["bash", "-s", "stable"],
                                       stdin=_getRVM.stdout)
    _getRVM.wait()
    subprocess.call(["source", "~/.rvm/scripts/rvm"])
    subprocess.call(["rvm", "requirements"])
    print _install

def rvmInstallRuby(version):
    subprocess.call(["rvm", "install", version])
    subprocess.call(["rvm", "use", "--default", version])
    subprocess.call(["rvm", "rubygems", "current"])

###########################################################################
# Actual Work
###########################################################################
# Configure Apt-Fast
addRepository("ppa:apt-fast/stable")
updateSlow()
installSlow("apt-fast")

# Add Keys, Update Software Listings
addGoogleKey()
addLine("/etc/apt/sources.list.d/google-chrome.list", "deb http://dl.google.com/linux/chrome/deb/ stable main")
addLine("/etc/apt/sources.list", "deb http://repository.spotify.com stable non-free")
addSpotifyKey()
addRepository("ppa:roggan87/nr")
addRepository("ppa:webupd8team/java")
enableAutoInstallJava()
update()

# Install Packages
install("net-responsibility")
install("emacs")
install("compizconfig-settings-manager")
install("nautilus-dropbox")

install("gitk")
install("vim")
install("subversion")
install("g++")
install("python3-minimal")
install("python-tk")
install("oracle-java7-installer")
install("default-jdk")

install("google-chrome-stable")
install("spotify-client")
install("texmaker")

installRvm()
rvmInstallRuby("1.9.3")
installHeroku()
install("ruby1.9.1-dev")
install("libpq-dev")
gemInstall("rdoc")
gemInstall("bundler")
gemInstall("rails")

install("sshpass")
install("openssh-server")
install("network-manager-openvpn-gnome")
install("adobe-flashplugin")

install("banshee")
install("gstreamer0.10-ffmpeg")
install("gstreamer0.10-fluendo-mp3")
install("ia32-libs")

install("preload")

# Kill Bloatware Processes
killall("deja-dup-monitor")
killall("ubuntuone-login")
killall("ubuntuone-preferences")
killall("ubuntuone-syncdaemon")
killall("orca")

# Kill Bloatware Files
rm_rf("~/.local/share/ubuntuone")
rm_rf("~/.cache/ubuntuone")
rm_rf("~/.config/ubuntuone")

# Purge Bloatware Packages
purge(["deja-dup"])
purge(["ubuntuone*", "python-ubuntuone-storage*"])
purge(["gnome-games-common", "gbrainy", "gnome-games-data", "aisleriot"])
purge(["shotwell"])
purge(["libreoffice-base", "libreoffice-math"])
purge(["gwibber*"])
purge(["remmina", "remmina-*"])
purge(["thunderbird*"])
purge(["transmission-daemon", "transmission-gtk"])
purge(["brasero"])
purge(["totem-gstreamer", "totem-common"])
purge(["rhythmbox", "rhythmbox-ubuntuone-music-store"])
purge(["bluez", "bluez-alsa", "bluez-cups", "bluez-gstreamer", "gir1.2-gnomebluetooth-1.0", "gnome-bluetooth", "pulseaudio-module-bluetooth"])
purge(["gnome-orca"])
purge(["onboard"])

# Update Software Listings
update()

# Clean Computer
autoclean()
clean()
autoremove()

# Update Software Listings after clean
update()
